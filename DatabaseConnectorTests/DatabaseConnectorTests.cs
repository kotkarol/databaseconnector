﻿using System;
using DatabaseConnector.DatabaseConnector;
using NUnit.Framework;
using Soneta.Business.App;

namespace DatabaseConnectorTests
{
    [TestFixture]
    public class DatabaseConnectorTests
    {
        [Test]
        public void GetDatabase_Good_DatabaseNotNull()
        {
            DatabaseConnectorRunTimeCompiled dcrc = new DatabaseConnectorRunTimeCompiled
            ("TEST",
                "Administrator",
                "");
            var data = (Database)dcrc.GetDatabase();

            //Assert.IsNotNull(data);
        }
        [Test]
        public void GetDatabase_Good_LoginNotNull()
        {
            DatabaseConnectorRunTimeCompiled dcrc = new DatabaseConnectorRunTimeCompiled
            ("TEST",
                "Administrator",
                "");
            var data = dcrc.GetLogin();
            //Assert.IsNotNull(data);
        }

    }
}
