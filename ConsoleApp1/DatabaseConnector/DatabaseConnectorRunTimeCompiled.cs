﻿using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Linq;
using System.Reflection;
using DatabaseConnector.Properties;
using Microsoft.CSharp;
using Soneta.Business.App;

namespace DatabaseConnector.DatabaseConnector
{
    public class DatabaseConnectorRunTimeCompiled
    {
        private readonly bool __constructorExecuted;
        private Login __login;
        private MsSqlDatabase __enovaDatabaseInstance;


        /// <summary>
        /// Default contructor, makes life easier. Allows you to connect enova365 database.
        /// </summary>
        /// <param name="sqlServer"></param>
        /// <param name="sqlUsername"></param>
        /// <param name="sqlPassword"></param>
        /// <param name="enova365Database"></param>
        /// <param name="enova365Username"></param>
        /// <param name="enova365Password"></param>
        /// <param name="pathToAssembly"></param>
        public DatabaseConnectorRunTimeCompiled(string enova365Database, string enova365Username, string enova365Password)
        {
            var check = CheckProvidedArguments(new[] { enova365Database, enova365Username, enova365Password });
            __databaseConnectorInstance = null;
            Enova365Database = enova365Database;
            Enova365Username = enova365Username;
            Enova365Password = enova365Password;
            PathToAssembly = GetPathToAssembly();
            StartRuntimeCompilation();
            __constructorExecuted = true;
        }
        /// <summary>
        /// Returns Enova365, which allows to use Enova365 bussines logic.
        /// </summary>
        /// <returns></returns>
        public Login GetLogin()
        {
            if (__login == null)
            {
                string getLoginMethodName = "GetEnova365LoginInstance";
                var instance = _databaseConnectorGetter;
                var method = instance.GetType().GetMethod(getLoginMethodName);

                if (method == null)
                {
                    throw new Exception($"ConsoleApp1.DatabaseConnector.GetLogin - has thrown exception: Method with name {getLoginMethodName} hasn't been found.");
                }
                var login = method.Invoke(instance, new object[] { });
                __login = (Login)method?.Invoke(instance, new object[] { });
            }

            return __login;
        }
        /// <summary>
        /// Returns SQL database connection information, made using information given in constructor.
        /// </summary>
        /// <returns></returns>
        public object GetDatabase()
        {
            if (__enovaDatabaseInstance == null)
            {
                var instance = _databaseConnectorGetter;
                var methods = ((instance?.GetType()?.GetMethods()) ?? throw new InvalidOperationException()).FirstOrDefault(x1 => x1.Name == "GetEnova365Database");
                __enovaDatabaseInstance = (MsSqlDatabase)methods?.Invoke(instance, new object[] { });
                return __enovaDatabaseInstance;
            }
            return __enovaDatabaseInstance;
        }

        public void LoginToExternalSqlServerDatabase(string sqlServerAdress, string sqlUsername, string sqlPassword)
        {

        }
        /// <summary>
        /// This property stores instance of type DatabaseConnector which is used to connect with Enova365.
        /// </summary>
        private object __databaseConnectorInstance;
        /// <summary>
        /// This property is getting instance of type DatabaseConnector which is used to connect with Enova365.
        /// </summary>
        private object _databaseConnectorGetter
        {
            get
            {
                if (__databaseConnectorInstance == null)
                {
                    if (!__constructorExecuted) return null;
                    var type = _compilerResults.CompiledAssembly.GetType("DatabaseConnector.DatabaseConnector.DatabaseConnector");
                    var constructor = type.GetConstructor(new[] { typeof(string), typeof(string), typeof(string) });
                    __databaseConnectorInstance = constructor?.Invoke(new object[] { Enova365Database, Enova365Username, Enova365Password });

                    var instance = __databaseConnectorInstance;
                    var methods = instance.GetType().GetMethods().Where(x1 => x1.Name == "GetEnova365LoginInstance").FirstOrDefault();
                    var login = methods.Invoke(instance, new Object[] { });
                }
                return __databaseConnectorInstance;
            }
            set => value = __databaseConnectorInstance;
        }
        /// <summary>
        /// Field which is used to to store compiled assembly
        /// </summary>
        private CompilerResults _compilerResults;

        /// <summary>
        /// Path to Enova365 installation folder, where are needed assemblies.
        /// </summary>
        private string PathToAssembly { get; set; }
        /// <summary>
        /// Password of Enova365 database operator.(isn't same as MS SQL server operator password)
        /// </summary>
        public string Enova365Password { get; private set; }
        /// <summary>
        /// Username of Enova365 database operator.(isn't same as MS SQL server operator)
        /// </summary>
        public string Enova365Username { get; private set; }
        /// <summary>
        /// Name of database of Enova365 which you want to acess.
        /// </summary>
        public string Enova365Database { get; private set; }
        /// <summary>
        /// This method returns all needed reference name with paths, which are needed to Login to Enova365 database.
        /// </summary>
        /// <param name="pathToEnova365InstallationFolder"></param>
        /// <returns></returns>
        private string[] GetMinimalReferencedAssembliesArray()
        {
            return new[]
            {
                "System.dll",
                "System.Data.dll",
                "System.Xml.dll",
                "System.Linq.dll",
                PathToAssembly + "Soneta.BI.dll",
                PathToAssembly + "Soneta.Business.dll",
                PathToAssembly + "Soneta.Core.dll",
                PathToAssembly + "Soneta.CRM.dll",
                PathToAssembly + "Soneta.Deklaracje.dll",
                PathToAssembly + "Soneta.Handel.dll",
                PathToAssembly + "Soneta.KadryPlace.dll",
                PathToAssembly + "Soneta.Kasa.dll",
                PathToAssembly + "Soneta.Ksiega.dll",
                PathToAssembly + "Soneta.Start.dll",
                PathToAssembly + "Soneta.Types.dll",
                PathToAssembly + "Soneta.Workflow.dll",
                PathToAssembly + "Soneta.Zadania.dll"
            };
        }
        /// <summary>
        /// This method start runtime compilation of files given in Settings under the names - you should place paths to those file there
        /// <para>PathToDatabaseConnectorDotCs</para>
        /// <para>PathToDatabaseConnectorExceptionsDotCs</para>
        /// </summary>
        public void StartRuntimeCompilation()
        {
            CSharpCodeProvider provider = new CSharpCodeProvider();
            CompilerParameters parameters = new CompilerParameters
            {
                GenerateInMemory = true,
                GenerateExecutable = false
            };

            var minimalList = GetMinimalReferencedAssembliesArray().ToList();
            minimalList.ForEach(assembly => parameters.ReferencedAssemblies.Add(assembly));

            string first = CheckIfNecessaryFileExits(Settings.Default.PathToDatabaseConnectorDotCs);
            string second = CheckIfNecessaryFileExits(Settings.Default.PathToDatabaseConnectorExceptionsDotCs);

            string databaseConnectorCode = GetTextFromFileInPath(first);
            string databaseConnectorExceptionsCode = GetTextFromFileInPath(second);
            _compilerResults = provider.CompileAssemblyFromSource(parameters, new[] { databaseConnectorCode, databaseConnectorExceptionsCode });

        }
        /// <summary>
        /// Checking if file in given path exists. In case if it not exits method throw exception with special message.
        /// </summary>
        /// <param name="path"></param>
        public static string CheckIfNecessaryFileExits(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new Exception(
                    "Provided path is null or empty! You should place your path to DatabaseConnector.cs file under the name PathToDatabaseConnectorDotCs."
                    + Environment.NewLine
                    + " You should place your path to DatabaseConnectorExceptions.cs file under the name PathToDatabaseConnectorExceptionsDotCs.");
            }
            if (!File.Exists(path))
            {
                path = path.Replace("D:", "E:");
            }
            if (!File.Exists(path))
            {
                throw new Exception(
                    "File to compilation do not exist! - DatabaseConnectorRunTimeCompiled.StartRuntimeCompilation()" + Environment.NewLine + "Looked file: " + path);
            }
            return path;
        }

        /// <summary>
        /// This method returns file content in string from given file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string GetTextFromFileInPath(string path)
        {
            if (!File.Exists(path)) return null;
            var x = File.OpenText(path);
            string databaseConnectorCode = x.ReadToEnd();
            return databaseConnectorCode;
        }
        /// <summary>
        /// This method checks if any of provided string is null.
        /// If it's null method throw exception of type DatabaseConstructorArgumentAreEmptyOrNullException.
        /// </summary>
        /// <param name="strings"></param>
        /// <returns></returns>
        private bool CheckProvidedArguments(string[] strings)
        {
            for (int i = 0; i < strings.Length; i++)
            {
                string s = strings[i];
                if (s == null) throw new DatabaseConstructorArgumentAreEmptyOrNullException();
            }
            return true;
        }
        /// <summary>
        /// Returing path to the Enova365.
        /// </summary>
        private string GetPathToAssembly()
        {

            var allAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
            var firstSonetaAssembly = allAssemblies.FirstOrDefault(x => x.Location.Split('\\').Last().StartsWith("Soneta"));

            if (firstSonetaAssembly == null)
            {
                throw new Exception($"ConsoleApp1.DatabaseConnector.DatabaseConnectorRunTimeCompiled.GetPathToAssembly - has thrown an Exception +" +
                                    $"any Soneta dll file isn't referenced to current project.");
            }

            var firstSonetaLocation = firstSonetaAssembly.Location;
            var pathToEnova = firstSonetaLocation.Replace(firstSonetaLocation.Substring(firstSonetaLocation.LastIndexOf('\\') + 1), "");
            if (string.IsNullOrEmpty(pathToEnova))
            {
                throw new Exception($"ConsoleApp1.DatabaseConnector.DatabaseConnectorRunTimeCompiled.GetPathToAssembly - has thrown an Exception +" +
                                    $"pathToEnova is null or empty.");
            }
            return pathToEnova;
        }
        /// <summary>
        /// Retruning Assembly with given name.
        /// Example GetAssemblyByName(Soneta.Bussines)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private Assembly GetAssemblyByName(string name)
        {
            return AppDomain.CurrentDomain.GetAssemblies().
                SingleOrDefault(assembly => assembly.GetName().Name == name);
        }
    }
}
