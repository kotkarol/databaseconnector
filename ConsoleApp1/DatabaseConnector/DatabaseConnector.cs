﻿using System;
using Soneta.Business.App;
using Soneta.Start;

namespace DatabaseConnector.DatabaseConnector
{
    internal sealed class DatabaseConnector : IDisposable
    {
        /// <summary>
        /// MS SQL Database Server name.
        /// </summary>
        public string SqlServer { get; private set; }

        /// <summary>
        /// MS SQL Database user name which allows to login.
        /// </summary>
        public string SqlUsername { get; private set; }

        /// <summary>
        /// MS SQL Database user password which allows to login.
        /// </summary>
        public string SqlPassword { get; private set; }

        /// <summary>
        /// Name of database of Enova365 which you want to acess.
        /// </summary>
        public string Enova365Database { get; private set; }

        /// <summary>
        /// Username of Enova365 database operator.(isn't same as MS SQL server operator)
        /// </summary>
        public string Enova365Username { get; private set; }

        /// <summary>
        /// Password of Enova365 database operator.(isn't same as MS SQL server operator password)
        /// </summary>
        public string Enova365Password { get; private set; }

        /// <summary>
        /// Connection to MSSQL database created basing at data provided at class contructor.
        /// </summary>
        public Database SqlDatabase
        {
            get { return GetEnova365Database(); }
        }

        /// <summary>
        /// Returns Database login which allow to create a session.
        /// </summary>
        public Login Enova365Login
        {
            get { return GetEnova365LoginInstance(); }

        }

        /// <summary>
        /// Defines if contructor has been executed without any issues.
        /// </summary>
        private bool __constructorExecuted;
        /// <summary>
        /// Instance of this class provides user to connect to MSSQL database and also proviedes to login to Enova365 program itself.
        /// </summary>
        /// <param name="sqlServer"></param>
        /// <param name="sqlUsername"></param>
        /// <param name="sqlPassword"></param>
        /// <param name="enova365Database"></param>
        /// <param name="enova365Username"></param>
        /// <param name="enova365Password"></param>
        public DatabaseConnector(string enova365Database, string enova365Username, string enova365Password)
        {
            var check = CheckProvidedArguments(new[] {enova365Database, enova365Username, enova365Password });
            if (!check) return;
            Enova365Database = enova365Database;
            Enova365Username = enova365Username;
            Enova365Password = enova365Password;
            __constructorExecuted = true;
        }
        /// <summary>
        /// Returns instance of MsSqlDatabase with all provided information in constructor
        /// </summary>
        /// <returns></returns>
        public Database GetEnova365Database()
        {
            try
            {
                if (!__constructorExecuted) throw new DatabaseConstructorArgumentAreEmptyOrNullException();
                return BusApplication.Instance[Enova365Database];
            }
            catch (Exception e)
            {
                throw new Exception("DatabaseConnector.GetEnova365Database: has thrown exception.", e);
            }
        }
        /// <summary>
        /// Returns instance of Soneta.Business.App.Login which is already connected to Enova365 bussines logic.
        /// </summary>
        /// <returns></returns>
        public Login GetEnova365LoginInstance()
        {
            try
            {
                if (!__constructorExecuted) throw new DatabaseConstructorArgumentAreEmptyOrNullException();
                LoadAssemblies();
                return SqlDatabase.Login(false, Enova365Username, Enova365Password);
            }
            catch (Exception e)
            {
                throw new Exception("DatabaseConnector.GetEnova365LoginInstance: has thrown exception.", e);
            }
        }

        private void LoadAssemblies()
        {
            Loader loader = new Loader();
            loader.WithExtensions = false;
            loader.Load();
        }

        /// </summary>
        /// <param name="strings"></param>
        private bool CheckProvidedArguments(string[] strings)
        {
            for (int i = 0; i < strings.Length; i++)
            {
                string s = strings[i];
                if (s == null) throw new DatabaseConstructorArgumentAreEmptyOrNullException();
            }
            return true;
        }

        public void Dispose()
        {
            if (Enova365Login != null)
            {
                Enova365Login.Dispose();
            }
        }
    }
}
