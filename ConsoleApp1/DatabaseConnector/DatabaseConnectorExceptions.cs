﻿using System;

namespace DatabaseConnector.DatabaseConnector
{
    public class DatabaseConnectorExceptions
    {
        
    }

    public class DatabaseConstructorArgumentAreEmptyOrNullException : Exception
    {
        public DatabaseConstructorArgumentAreEmptyOrNullException() : base("DatabaseConnector: One or more of DatabaseConnector contrucor arguments is null or empty!")
        {
        }

        public DatabaseConstructorArgumentAreEmptyOrNullException(string message)
            : base(message)
        {
        }

        public DatabaseConstructorArgumentAreEmptyOrNullException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    public class DatabaseConnectorInstaceHaventCreatedException : Exception
    {
        public DatabaseConnectorInstaceHaventCreatedException() : base("DatabaseConnector: At first you should create instance of DatabaseConnector where you will provide all necessery data!")
        {
        }

        public DatabaseConnectorInstaceHaventCreatedException(string message)
            : base(message)
        {
        }

        public DatabaseConnectorInstaceHaventCreatedException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
